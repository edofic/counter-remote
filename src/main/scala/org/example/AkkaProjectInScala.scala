package org.example

import akka.actor._
import akka.pattern.ask
import scala.concurrent.duration._
import akka.util.Timeout
import scala.util._

case object Tick
case object Get

class Counter extends Actor {
  var count = 0

  val id = math.random.toString.substring(2)
  println(s"\nmy name is $id\ni'm at ${self.path}\n")
  def log(s: String) = println(s"$id: $s")

  def receive = {
    case Tick =>
      count += 1
      log(s"got a tick, now at $count")
    case Get  =>
      sender ! count
      log(s"asked for count, replied with $count")
  }
}

object AkkaProjectInScala extends App {
  val system = ActorSystem("ticker")
  implicit val ec = system.dispatcher

  implicit class SingleActorSystem(sys: ActorSystem){
    def actorSingle(name: String, props: => Props) =
      Try(sys.settings.config getString s"akka.remote.path.$name").
        map(sys.actorFor).
        getOrElse(sys.actorOf(Props[Counter], name))
  }

  val counter = system.actorSingle("counter", Props[Counter])

  def step {
    print("tick or quit? ")
    readLine() match {
      case "tick" => counter ! Tick
      case "quit" => return
      case _ =>
    }
    step
  }
  step

  implicit val timeout = Timeout(5.seconds)

  val f = counter ? Get
  f onComplete {
    case Failure(e) => throw e
    case Success(count) => println("Count is " + count)
  }

  system.shutdown()
}

